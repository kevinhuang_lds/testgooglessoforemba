package tw.com.ischool.testgooglecookieforemba;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class GoogleSignInActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_sign_in);

        WebView myWebView = (WebView) findViewById(R.id.webView);
        new WebviewDecorator(myWebView);

//        String url = "https://mail.google.com";
        String url = "https://web2.ischool.com.tw";
        myWebView.loadUrl(url);
    }
}
