package tw.com.ischool.testgooglecookieforemba;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class RoomActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_sign_in);

        WebView myWebView = (WebView) findViewById(R.id.webView);
        new WebviewDecorator(myWebView);

        String url = "https://sites.google.com/a/emba.ntu.edu.tw/student/tao-lun-xiao-jian-shen-qing";
        myWebView.loadUrl(url);
    }
}
