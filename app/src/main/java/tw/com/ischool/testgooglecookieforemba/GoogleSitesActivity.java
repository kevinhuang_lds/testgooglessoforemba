package tw.com.ischool.testgooglecookieforemba;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class GoogleSitesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_sign_in);

        WebView myWebView = (WebView) findViewById(R.id.webView);

        new WebviewDecorator(myWebView);

        myWebView.loadUrl("https://sites.google.com/a/emba.ntu.edu.tw/student/annonce");
    }

}
